import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { DataItem, GroupCategoryData } from '../tableData';

@Component({
  selector: 'table-category',
  templateUrl: './groupCategory.component.html',
  styleUrls: ['./groupCategory.component.scss']
})
export class GroupCategoryComponent implements OnInit {
  searchValue = '';
  visible = false;

  checked = false;
  indeterminate = false;
  isConfirmLoading = false;
  isDetail = false;
  isCreate = false;

  // ===== Select Options ===== //
  expandSet = new Set<number>();

  // ===== Table ===== //
  listOfCurrentPageData: DataItem[] = [];
  setOfCheckedId = new Set<number>();

  listOfData: DataItem[] = GroupCategoryData;
  categoryData = [...this.listOfData];

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.visible = false;
    this.categoryData = this.listOfData.filter((item: DataItem) => item.category.toLocaleLowerCase().indexOf(this.searchValue) !== -1);
  }

  deleteRow(id: number): void {
    this.categoryData = this.categoryData.filter((d) => d.id !== id);
    this.messageService.info('data has been deleted');
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach((item) =>
      this.updateCheckedSet(item.id, value)
    );
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: DataItem[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every((item) =>
      this.setOfCheckedId.has(item.id)
    );
    this.indeterminate =
      this.listOfCurrentPageData.some((item) =>
        this.setOfCheckedId.has(item.id)
      ) && !this.checked;
  }


  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  constructor(private messageService: NzMessageService) { }

  ngOnInit() {
  }

  onChange($event: string[]): void {
    console.log($event);
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    this.isDetail = true;

    setTimeout(() => {
      this.isDetail = false;
      this.isConfirmLoading = false;
      this.messageService.info('data confirmed');
    }, 3000);
  }

  createSubmit(): void {
    this.isConfirmLoading = true;
    this.isCreate = true;

    setTimeout(() => {
      this.isCreate = false;
      this.isConfirmLoading = false;
      this.messageService.info('new group data has been created');
    }, 3000);
  }

  handleCancel(): void {
    this.isDetail = false;
    this.isCreate = false;
  }

  detailOpen(): void {
    this.isDetail = true;
  }

  createOpen(): void{
    this.isCreate = true
  }
}
