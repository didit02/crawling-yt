export const pieOptions = {
  colors:['#06d6a0','#ff6b6b','#1990ff','#ffe66d','#9381ff'],
  chart: {
    width: 390,
    type: "donut"
  },
  dataLabels: {
    enabled: false
  },
  labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
  responsive: [
    {
      breakpoint: 769,
      options: {
        chart: {
          width: 310,
        },
        legend: {
          offsetY: 25,
        }
      }
    },
    {
      breakpoint: 1025,
      options: {
        chart: {
          width: 340
        },
      }
    },
  ]
};
