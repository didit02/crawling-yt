import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiService } from "../../services/api.service";
@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(
    private api : ApiService
  ) { }
  
  postParams(data:any): Observable<any> {
    const url = 'http://206.189.89.203:8080/api/search/youtube';
    const body = data;
    const header = {
      "Content-Type" : "application/json",
      "Authorization" : 'Token 9c37480fbf200ceecc153ea57f63147c63595772' 
    }
    return this.api.postdatacustomHeader(url, body , header);
  }
  postParamsDetail(data:any): Observable<any> {
    const url = 'http://206.189.89.203:8080/api/search/youtube/comment';
    const body = data;
    const header = {
      "Content-Type" : "application/json",
      "Authorization" : 'Token 9c37480fbf200ceecc153ea57f63147c63595772' 
    }
    return this.api.postdatacustomHeader(url, body , header);
  }
  getParamsDetail(data:any): Observable<any> {
    const url = 'http://206.189.89.203:8080/api/list/youtube/'+data;
    const header = {
      "Content-Type" : "application/json",
      "Authorization" : 'Token 9c37480fbf200ceecc153ea57f63147c63595772' 
    }
    return this.api.getdatacustomHeader(url, header);
  }
  postDataCrawling(data:any): Observable<any> {
    const url = 'http://206.189.89.203:8080/api/crawling/youtube';
    const body = data;
    const header = {
      "Content-Type" : "application/json",
      "Authorization" : 'Token 9c37480fbf200ceecc153ea57f63147c63595772' 
    }
    return this.api.postdatacustomHeader(url, body , header);
  }
}
