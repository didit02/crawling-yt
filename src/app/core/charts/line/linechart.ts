export const lineOptions = {
  colors:['#06d6a0','#ff6b6b','#1990ff','#ffe66d','#36414c'],
  chart: {
    type: "line",
    height: 273,
    width: "100%",
    zoom: {
      enabled: false
    },
    dropShadow: {
      enabled: true,
      enabledOnSeries: undefined,
      top: 5,
      left: 0,
      blur: 6,
      color: 'rgba(150, 170, 180, 1)',
      opacity: 0.3
    },
    toolbar: {
      show: false,
      offsetX: 0,
      offsetY: 0,
      tools: {
        download: true,
        selection: true,
        zoom: true,
        zoomin: true,
        zoomout: true,
        pan: true,
        reset: true,
        customIcons: []
      },
      export: {
        csv: {
          filename: undefined,
          columnDelimiter: ',',
          headerCategory: 'category',
          headerValue: 'value',
          dateFormatter(timestamp) {
            return new Date(timestamp).toDateString()
          }
        }
      },
      autoSelected: 'zoom'
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    curve: "straight",
    lineCap: 'round',
    width: 3
  },
  markers: {
    size: 6,
    strokeColors: '#44525f',
    hover: {
      sizeOffset: 2
    }
  },
  fill: {
    opacity: 0.3
  },
  tooltip: {
    y: [
      {
        title: {
          formatter: function(val) {
            return val;
          }
        }
      }
    ]
  },
  grid: {
    borderColor: "#DAE0E7",
  },
  legend: {
    floating: false,
    fontWeight: 600,
    itemMargin: {
      horizontal: 5,
      vertical: 0
    },
  }
};
