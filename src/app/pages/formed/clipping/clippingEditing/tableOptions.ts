import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';

export interface DataItem {
  id: number;
  date: string;
  media: string;
  link: string;
  linkFull?: string;
  summary?: string;
  headline: string;
  sentiment?: string;
}

export interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder | null;
  sortFn?: NzTableSortFn | null;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn | null;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
  width?: string;
  align?: string
}


export const tableCol: ColumnItem[] = [
  {
    name: 'Date',
    width: '20%',
    align: 'left',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.date.localeCompare(b.date),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Media',
    width: '20%',
    align: 'left',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.media.localeCompare(b.media),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Headline',
    width: '30%',
    align: 'left',
  },
  {
    name: 'Tone',
    width: '20%',
    align: 'left',
    sortOrder: 'descend',
    sortFn: (a: DataItem, b: DataItem) => a.sentiment.localeCompare(b.sentiment),
    sortDirections: ['ascend', 'descend', null],
    filterMultiple: true,
    listOfFilter: [
      { text: 'Positive', value: 'positive' },
      { text: 'Neutral', value: 'neutral' },
      { text: 'Negative', value: 'negative' },
    ],
    filterFn: (list: string[], item: DataItem) => list.some(sentiment => item.sentiment.indexOf(sentiment) !== -1)
  }
];

export const tableData: DataItem[] = [
  {
    id: 1,
    sentiment: 'neutral',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kompas',
    linkFull: 'https://www.youtube.com/embed/Wf0lQIGwrWs',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 2,
    sentiment: 'negative',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kumparan',
    linkFull: 'https://kumparan.com/kumparannews/siapa-anarko-yang-sering-disebut-polisi-dalam-setiap-kerusuhan-1uMOzGEEY5s',
    summary:'Polisi memastikan hampir 800 anggota diduga kelompok anarko ditangkap dalam kerusuhan demo tolak Omnibus Law. Menurut polisi, mereka ditangkap lantaran merusak fasilitas publik serta menyasar kendaraan dinas kepolisian, pos polisi, hingga ambulans. ',
    headline: 'Siapa Anarko yang Sering Disebut Polisi dalam Setiap Kerusuhan?',
  },
  {
    id: 3,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 4,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 5,
    sentiment: 'neutral',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kompas',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 6,
    sentiment: 'negative',
    date: '02/01/2020',
    link:'mediasource.com',
    media: 'kumparan',
    linkFull: 'https://kumparan.com/kumparannews/jokowi-saya-melihat-unjuk-rasa-uu-cipta-kerja-didasari-hoaks-di-media-sosial-1uMHRpIAZZw',
    summary:'Presiden Jokowi turut memperhatikan adanya demonstrasi besar-besaran berujung ricuh menolak UU Omnibus Law Cipta Kerja. Kata Jokowi, buruh atau mahasiswa yang berdemo ada yang karena termakan hoaks.',
    headline: 'Jokowi: Saya Melihat Unjuk Rasa UU Cipta Kerja Didasari Hoaks di Media Sosial',
  },
  {
    id: 7,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 8,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 9,
    sentiment: 'neutral',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kompas',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 10,
    sentiment: 'neutral',
    date: '03/01/2020',
    link:'mediasource.com',
    media: 'kumparan',
    linkFull: 'https://kumparan.com/kumparannews/strategi-as-hadapi-china-di-balik-undangan-prabowo-ke-negeri-paman-sam-1uMY8ZoJOjV',
    summary:'Menteri Pertahanan, Prabowo Subianto, bakal mengunjungi Amerika Serikat (AS) pada 15-19 Oktober. Ia kini tak lagi masuk daftar hitam (blacklist) pemerintah AS setelah 20 tahun lamanya.',
    headline: 'Strategi AS Hadapi China di Balik Undangan Prabowo ke Negeri Paman Sam',
  },
  {
    id: 11,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 12,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 13,
    sentiment: 'neutral',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kompas',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 14,
    sentiment: 'positive',
    date: '04/01/2020',
    link:'mediasource.com',
    media: 'kumparan',
    linkFull: 'https://kumparan.com/kumparannews/trump-klaim-sembuh-dari-corona-berkat-regeneron-saya-menyebutnya-obat-covid-19-1uMTWPzpukT',
    summary:'Presiden Amerika Serikat, Donald Trump, Belum Bisa Dipastikan Apakah Sudah Sembuh Dari Corona. Akan Tetapi, Trump Sudah Diizinkan Pulang Dari RS Militer Walter Reed Dan Mengklaim Sudah Sehat Berkat Regeneron. Regeneron Merupakan Obat Antibodi Eksperimental Yang Terbuat Dari Cocktail. Nama Obat Itu Adalah REGN-COV2, Dikembangkan Oleh Perusahaan Bioteknologi Regeneron Pharmaceuticals.',
    headline: 'Trump Klaim Sembuh dari Corona Berkat Regeneron: Saya Menyebutnya Obat COVID-19',
  },
  {
    id: 14,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 14,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 15,
    sentiment: 'neutral',
    date: '01/01/2020',
    link:'mediasource.com',
    media: 'kompas',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 16,
    sentiment: 'positive',
    date: '05/01/2020',
    link:'mediasource.com',
    media: 'kumparan',
    linkFull: 'https://kumparan.com/kumparannews/fakta-fakta-penembakan-rombongan-tgpf-bentukan-mahfud-md-oleh-kkb-papua-1uMNTCVx4hD',
    summary:'Menko Polhukam, Mahfud MD, telah membentuk Tim Gabungan Pencari Fakta (TGPF) untuk mengusut kasus penembakan di Kabupaten Intan Jaya, Papua, yang terjadi 19 September. Insiden tersebut menewaskan Pendeta bernama Yeremia, dua anggota TNI, serta seorang warga sipil. ',
    headline: 'Fakta-fakta Penembakan Rombongan TGPF Bentukan Mahfud MD oleh KKB Papua',
  },
  {
    id: 17,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 18,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 19,
    sentiment: 'neutral',
    date: '03/01/2020',
    link: 'mediasource.com',
    media: 'tribunnews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 20,
    sentiment: 'neutral',
    date: '04/01/2020',
    link: 'mediasource.com',
    media: 'medianews',
    linkFull: 'mediasource.com/news-page/berita-baru-tahun-2020-akan-segera-muncul-tanpa-adanya-perihal-suatu-perintah-kementerian',
    summary:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, nisi eligendi inventore id exercitationem vero. Itaque veritatis accusamus illo voluptatibus!',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  }
];
