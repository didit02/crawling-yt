/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ClippingListComponent } from './clippingList.component';

describe('ClippingListComponent', () => {
  let component: ClippingListComponent;
  let fixture: ComponentFixture<ClippingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClippingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClippingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
