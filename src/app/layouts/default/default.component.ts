import { Component, OnInit } from '@angular/core';
import { endOfWeek, endOfMonth, endOfYear } from 'date-fns';

import { MenuItem } from '@constant/menuItem';

export const contentOpt = [
  { label: 'Title', value: 'title'},
  { label: 'Content', value: 'content'},
];

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  isDetail = false;
  sideCollapsed = false;
  menus = MenuItem;
  contentOptions = contentOpt;

  clientLogo = '../../../assets/img/client/';
  header = {
    src:'../../../assets/img/side-logo.png',
    label: 'dashboard',
  };

  ranges = {
    Today: [new Date(), new Date()],
    Week: [new Date(), endOfWeek(new Date())],
    Month: [new Date(), endOfMonth(new Date())],
    Year: [new Date(), endOfYear(new Date())],
  };

  constructor() { }

  ngOnInit() {

  }

  onChange(result: Date[]): void {
    console.log('From: ', result[0], ', to: ', result[1]);
  }

  openDetail(): void {
    this.isDetail = true
  }

  closeDetail(): void {
    this.isDetail = false
  }

}
