import { Component, OnInit, ViewEncapsulation } from '@angular/core';

export const groupOpt = [
  { label: 'Jack', value: 'jack', groupLabel: 'Manager' },
  { label: 'Lucy', value: 'lucy', groupLabel: 'Manager' },
  { label: 'Tom', value: 'tom', groupLabel: 'Engineer' },
  { label: 'Jon', value: 'jon', groupLabel: 'Engineer' }
];

export const contentOpt = [
  { label: 'Title', value: 'title'},
  { label: 'Content', value: 'content'},
];

export const mediaOpt = [
  { label: 'All Media', value: 'all'},
  { label: 'Media Cetak', value: 'Cetak' },
  { label: 'Media Online', value: 'Online'},
  { label: 'Media TV', value: 'TV' },
  { label: 'Media Radio', value: 'Radio'},
];


@Component({
  selector: 'app-clippingSearch',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './clippingSearch.component.html',
  styleUrls: ['./clippingSearch.component.scss']
})
export class ClippingSearchComponent implements OnInit {
  inputValue?: string;
  filteredOptions: string[] = [];
  loading = false;
  options = ['article', 'article', 'article'];
  data = [];

  groupOptions = groupOpt;
  contentOptions = contentOpt;
  mediaOptions = mediaOpt;


  change(): void {
    this.loading = true;
    if (this.data.length > 0) {
      setTimeout(() => {
        this.data = [];
        this.loading = false;
      }, 1000);
    } else {
      setTimeout(() => {
        this.data = [
          {
            title: 'Ant Design Title 1'
          },
          {
            title: 'Ant Design Title 2'
          },
          {
            title: 'Ant Design Title 3'
          },
          {
            title: 'Ant Design Title 4'
          },
          {
            title: 'Ant Design Title 5'
          }
          ,
          {
            title: 'Ant Design Title 6'
          }
        ];
        this.loading = false;
      }, 1000);
    }
  }

  constructor() {
    this.filteredOptions = this.options;
  }

  onChange(value: string): void {
    this.filteredOptions = this.options.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  ngOnInit() {

  }

}
