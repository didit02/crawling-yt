import { Injectable, OnInit} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse,HttpHeaders,HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService implements OnInit {
  private apiBaseUrl: string;
  constructor(
    private http: HttpClient,
    ) {
    this.apiBaseUrl = environment.apiUrl;
  }

  ngOnInit(): void {
  }
  setUrl(url: string): void {
    this.apiBaseUrl = url;
  }
  getBaseUrlAPI(): string {
    return this.apiBaseUrl;
  }
  apiUrl(url: string): string {
    if (url.includes('+'))
      url = url.replace(/\+/g, '%2B');
    return `${this.apiBaseUrl}/${url}`;
  }
  getData(url: string, params = {}): Observable<any> {
    return new Observable(observer => {
        const httpParams = new HttpParams();
        for (const key in params) if (params.hasOwnProperty(key)) httpParams.set(key, params[key]);
        const getUrl = url.includes('http') ? url : this.apiUrl(url);
        this.http.get(getUrl, {
          observe: 'response',
          params: httpParams
        }).subscribe(
          res => {
            observer.next(res.body);
          },
          (err: HttpErrorResponse) => {
            this.handleError(err);
            observer.error(err);
          },
          () => {
            observer.complete();
          }
        );

    });
  }
  postdatacustomHeader(url: string, body = '', header?: any): Observable<any> {
    return new Observable(observer => {
      const postUrl = url.includes('http') ? url : this.apiUrl(url);
      this.http.post(postUrl, body, {
        headers: new HttpHeaders(header),
        observe: 'response'
      }).subscribe(
        res => {
          observer.next(res.body);
        },
        (err: HttpErrorResponse) => {
          this.handleError(err);
          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
    });
}
getdatacustomHeader(url: string, header?: any): Observable<any> {
  return new Observable(observer => {
    const postUrl = url.includes('http') ? url : this.apiUrl(url);
    this.http.get(postUrl, {
      headers: new HttpHeaders(header),
      observe: 'response'
    }).subscribe(
      res => {
        observer.next(res.body);
      },
      (err: HttpErrorResponse) => {
        this.handleError(err);
        observer.error(err);
      },
      () => {
        observer.complete();
      }
    );
  });
}
  handleError(err: HttpErrorResponse): void {
    switch (err.status) {
      case 0:
        //koneksi lose
         break;
      case 500:
        //internal server error
      default:;
    }
  }
}
