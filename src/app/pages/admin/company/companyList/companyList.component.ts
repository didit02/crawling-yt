import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

import { NzUploadFile } from 'ng-zorro-antd/upload';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';

import { tableCol, tableData } from '../tableOptions';
import { MediaItem } from '../mediaSelect';

import { lorem } from '@constant/loremIpsum';

@Component({
  selector: 'app-companyList',
  templateUrl: './companyList.component.html',
  styleUrls: ['./companyList.component.scss']
})
export class CompanyListComponent implements OnInit {
  checked = false;
  indeterminate = false;
  isDetail = false;

  isConfirmDetail = false;

  listOfCurrentPageData = tableData;
  isCheckedId = new Set<number>();

  listOfColumn = tableCol;
  listOfData = tableData;

  lorem = lorem;

  // Media Selection
  mediaSearch = '';
  mediaExpand = ['online', 'koran', 'majalah', 'radio', 'tv', 'tabloid'];
  MediaSelect = MediaItem;

  statusOpt = [
    { label: 'Active', value: 'active'},
    { label: 'InActive', value: 'inactive' },
  ];

  defaultFileList: NzUploadFile[] = [
    {
      uid: '1',
      name: 'indosat.png',
      status: 'done',
      url: '../../../../assets/img/client/indosat.png',
    }
  ];
  uploadList = [...this.defaultFileList];

  buttonItem = [
    {
      text: 'download',
      icon: 'download',
      class: 'line-info'
    },
    {
      text: 'delete',
      icon: 'delete',
      class: 'line-danger'
    }
  ];

  deleteRow(id: number): void {
    this.listOfData = this.listOfData.filter((d) => d.id !== id);
    this.messageService.info('data has been deleted');
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.isCheckedId.add(id);
    } else {
      this.isCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach((item) =>
      this.updateCheckedSet(item.id, value)
    );
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange(tableData): void {
    this.listOfCurrentPageData = tableData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every((item) =>
      this.isCheckedId.has(item.id)
    );
    this.indeterminate =
      this.listOfCurrentPageData.some((item) =>
        this.isCheckedId.has(item.id)
      ) && !this.checked;
  }

  constructor(private messageService: NzMessageService) { }

  ngOnInit() {
  }

  onSelection(event: NzFormatEmitEvent): void {
    console.log(event);
  }

  openDetail() {
    this.isDetail = true;
  }

  confirmDetail(type: string) {
    this.isConfirmDetail = true;
    setTimeout(() => {
      this.isDetail = false;
      this.isConfirmDetail = false;
      this.messageService.create(type, `company detail update ${type}`);
    }, 3000);
  }

  closeDetail() {
    this.isDetail = false;
  }

}
