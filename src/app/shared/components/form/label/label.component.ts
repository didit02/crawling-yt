import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[label]',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
  @Input() text: string;
  @Input() for: string;

  constructor() { }

  ngOnInit() {
  }

}
