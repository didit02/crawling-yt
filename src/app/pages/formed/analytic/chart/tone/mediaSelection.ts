import { barOptions } from '@core/charts/bar/barchart';
import { mockData1, mockData2, mockData3, mockData4 } from '@core/data/widgetData';

export const mediaSelection = {
  colors:[ '#ff6b6b', '#1990ff', '#06d6a0'],
  series: [
    {
      name: "Negative",
      data: mockData1,
    },
    {
      name: 'Neutral',
      data: mockData3,
    },
    {
      name: "Positive",
      data: mockData4,
    },
  ],
  chart: barOptions.chart,
  plotOptions: barOptions.plotOptions,
  stroke: barOptions.stroke,
  xaxis: {
    categories: ['media.com', 'media.com', 'media.com', 'media.com', 'media.com', 'media.com', 'media.com', 'media.com', 'media.com', 'media.com'],
    labels: barOptions.xaxis.labels
  },
  yaxis: barOptions.yaxis,
  tooltip: barOptions.tooltip,
  fill: barOptions.fill,
  legend:barOptions.legend
}
