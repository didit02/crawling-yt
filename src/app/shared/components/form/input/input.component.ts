import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input[nz-input]',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @Input() type: string = 'text';
  @Input() size: string;

  @Input() disabled: boolean = false;
  @Input() bordered: boolean;

  @Input() label: string;
  @Input() placeholder: string;

  @Input() icon: string;
  @Input() extraIcon: string;

  @Input() value?: string;
  @Input() autoComplete: string;
  @Output() onChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
