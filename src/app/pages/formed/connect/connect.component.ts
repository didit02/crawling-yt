import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { NzUploadFile, NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { NzMessageService } from 'ng-zorro-antd/message';

import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent implements OnInit {

  blured = false
  focused = false

  isVisible = false;
  isConfirmLoading = false;
  indeterminate = false;

  allChecked = false;
  checkMedia = [
    { label: 'Media 1', value: 'Media 1', checked: false },
    { label: 'Media 2', value: 'Media 2', checked: false },
    { label: 'Media 3', value: 'Media 3', checked: false },
    { label: 'Media 4', value: 'Media 4', checked: false },
    { label: 'Media 5', value: 'Media 5', checked: false },
    { label: 'Media 6', value: 'Media 6', checked: false },
    { label: 'Media 7', value: 'Media 7', checked: false },
    { label: 'Media 8', value: 'Media 8', checked: false },
    { label: 'Media 9', value: 'Media 9', checked: false },
    { label: 'Media 10', value: 'Media 10', checked: false },
    { label: 'Media 11', value: 'Media 11', checked: false },
    { label: 'Media 12', value: 'Media 12', checked: false }
  ];

  checkEditorial = [
    { label: 'National', value: 'National', checked: true },
    { label: 'Bussiness', value: 'Bussiness' },
    { label: 'Legal', value: 'Legal' }
  ];

  created(event) {
    console.log('editor-created', event)
  }

  changedEditor(event: EditorChangeContent | EditorChangeSelection) {
    console.log('editor-change', event)
  }

  focus($event) {
    console.log('focus', $event)
    this.focused = true
    this.blured = false
  }

  blur($event) {
    console.log('blur', $event)
    this.focused = false
    this.blured = true
  }

  log(value: object[]): void {
    console.log(value);
  }

  constructor(private http: HttpClient, private messageService: NzMessageService) { }

  previewFile = (file: NzUploadFile) => {
    console.log('Your upload file:', file);
    return this.http
      .post<{ thumbnail: string }>(`https://next.json-generator.com/api/json/get/4ytyBoLK8`, {
        method: 'POST',
        body: file
      })
      .pipe(map(res => res.thumbnail));
  };

  handleChange({ file, fileList }: NzUploadChangeParam): void {
    const status = file.status;
    if (status !== 'uploading') {
      console.log(file, fileList);
    }
    if (status === 'done') {
      this.messageService.success(`${file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      this.messageService.error(`${file.name} file upload failed.`);
    }
  }

  updateAllChecked(): void {
    this.indeterminate = false;
    if (this.allChecked) {
      this.checkMedia = this.checkMedia.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.checkMedia = this.checkMedia.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
  }

  updateSingleChecked(): void {
    if (this.checkMedia.every(item => !item.checked)) {
      this.allChecked = false;
      this.indeterminate = false;
    } else if (this.checkMedia.every(item => item.checked)) {
      this.allChecked = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
    }
  }

  handleConnect(type: string): void {
    this.isConfirmLoading = true;

    setTimeout(() => {
      this.isVisible = false;
      this.isConfirmLoading = false;
      this.messageService.create(type, `connect data ${type}`);
    }, 3000);
  }

  handleCancel(): void {
    console.log('cancel button');
  }


  ngOnInit() {

  }

}
