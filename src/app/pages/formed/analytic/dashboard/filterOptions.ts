export const contentOpt = [
  { label: 'Daily', value: 'daily'},
  { label: 'Weekly', value: 'weekly' },
  { label: 'Monthly', value: 'monthly'},
  { label: 'Yearly', value: 'yearly'},
];

export const mediaOpt = [
  { label: 'All Media', value: 'allmedia'},
  { label: 'Media Cetak', value: 'cetak' },
  { label: 'Media Online', value: 'online'},
  { label: 'Media TV', value: 'tv' },
  { label: 'Media Radio', value: 'radio'},
];

export const subMediaOpt = [
  { label: 'All SubMedia', value: 'allsubmedia' },
];

export const categoryOpt = [
  { label: 'Category 1', value: 'category1' },
  { label: 'Category 2', value: 'category2' },
  { label: 'Category 3', value: 'category3' },
  { label: 'Category 4', value: 'category4' },
];

export const subCategoryOpt = [
  { label: 'Sub Category 1', value: 'subcategory1' },
  { label: 'Sub Category 2', value: 'subcategory2' },
  { label: 'Sub Category 3', value: 'subcategory3' },
  { label: 'Sub Category 4', value: 'subcategory4' },
];
