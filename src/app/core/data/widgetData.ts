export const WidgetData = [
  47, 45, 54,
  38, 56, 24,
  65, 31, 37,
  39, 62, 51,
  47, 45, 54,
  38, 56, 24,
  65, 31, 37,
  39, 62, 51
];

export const mockData1 = [45, 52, 38, 24, 33, 26, 21, 20, 6, 8, 15, 10];
export const mockData2 = [35, 41, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35];
export const mockData3 = [34, 44, 54, 21, 12, 43, 33, 23, 66, 66, 58, 15];
export const mockData4 = [47, 45, 54, 38, 56, 24, 65, 31, 37, 39, 62, 51];

export const setData1 = [0, 17, 18, 7, 6, 11, 5, 8, 12, 19, 4, 13];
export const setData2 = [15, 4, 8, 7, 9, 5, 10, 13, 14, 0, 3, 18];
export const setData3 = [5, 17, 7, 13, 1, 19, 14, 6, 9, 18, 8, 16];
export const setData4 = [19, 4, 13, 14, 8, 18, 6, 2, 17, 11, 12, 5];
export const setData5 = [7, 3, 20, 12, 9, 1, 10, 17, 16, 8, 19, 2];
