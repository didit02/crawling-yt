import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() options: [];
  @Input() maxTag: string;

  @Input() mode: string;
  @Input() placeholder: string;
  constructor() { }

  ngOnInit() {
  }

}
