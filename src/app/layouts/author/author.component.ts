import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthorService } from "../author/author.service";
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  isDetail = false;
  loading = false;
  data: any[] = [];
  datapush = []
  commentData = []
  // commentData = [
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  //   {
  //     author: 'Han Solo',
  //     avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
  //     content:
  //       'We supply a series of design principles, practical patterns and high quality design resources' +
  //       '(Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  //   },
  // ];

  searchdata: any;
  totaldata:any;
  totaldatadetail:any;
  jumlahcomment:any;
  vidioId:any;
  vidioembed:any
  detaildes:any;
  constructor(
    private authorSvc: AuthorService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    // this.loadData(1);
  }

  onSearch(value: any) {
    this.searchdata = value
    if(this.searchdata.includes('youtube')) this.pushdatacrawling()
    else this.loadData(1);
  }

  onDetail(data:any) {
    this.isDetail = true;
    this.vidioId = data
    this.loadDataDetail(1)
  }

  onClose() {
    this.isDetail = false;
  }

  pushdatacrawling(){
   const params ={
    "cid" : this.searchdata,
    "cname": ""
   } 
   this.authorSvc.postDataCrawling(params).subscribe(
    res=> {
      if (res){
          alert(res.message)
      }
    }
  )
  }
  loadDataDetail(page:number) : void{
    const params = {
      "video_id" : this.vidioId,
      "page": page - 1
    }
    this.authorSvc.postParamsDetail(params).subscribe(
      res=> {
        this.totaldatadetail = res.total_data
        this.vidioembed = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+res.video[0].video_id)
        this.detaildes = {
          title : res.video[0].video_desc,
          like : res.video[0].likes_count,
          dislike : res.video[0].dislike_count,
          comment : res.video[0].comment_count,
          date : res.video[0].publish_time
        }
        this.commentData = []
        res.comment.forEach(element => {
          const obj = {
            author: element.author,
            avatar: element.author_profile,
            content: element.text,
            like: element.likes_count,
          }
          this.commentData.push(obj)
        });
        
        
          // console.log(this.jumlahcomment);
          // res.comment.forEach(element => {
          //     const dataobj = {
          //       author: element.author,
          //       avatar: element.author_profile,
          //       content: element.text,
          //       likes: element.likes_count,
          //     }
          //     this.data.push(dataobj)
          // });
      }
    )
  }

  loadData(page: number): void {
    const params = {
      "channel_name": this.searchdata,
      "page": page -  1 
    }
    this.authorSvc.postParams(params).subscribe(
      result => {
        this.totaldata = result.total_data
        this.datapush = []
        result.data.forEach(element => {
          const dataobj = {
            href: element.post_url,
            title: element.video_title,
            video: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+element.video_id),
            channelName: element.channel_name,
            content: element.video_desc,
            like: element.likes_count,
            dislike: element.dislike_count,
            comment: element.comment_count,
            vid: element.video_id
          };
          this.datapush.push(dataobj)
        });
      }
    )
    // this.data = new Array(8).fill({}).map((_, index) => {
    //   return {
    //     href: 'http://ant.design',
    //     title: `video number ${index} (page: ${page}): Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odit, saepe.`,
    //     // video: 'https://www.youtube.com/embed/xFyIKupuGCM',
    //     channelName: 'channel name',
    //     content:
    //       'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.'
    //   };
    // });
  }
}
