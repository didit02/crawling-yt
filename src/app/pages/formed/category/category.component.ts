import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  isCreate = false;
  isConfirmLoading = false;
  constructor(private messageService: NzMessageService) { }

  ngOnInit(): void { }

  openCreate() {
    this.isCreate = true;
  }

  submitCreate(type: string) {
    this.isConfirmLoading = true;

    setTimeout(() => {
      this.isCreate = false;
      this.isConfirmLoading = false;
      this.messageService.create(type, `Create Group ${type}`);
    }, 3000);
  }

  closeCreate() {
    this.isCreate = false;
  }
}
