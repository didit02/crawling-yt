import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';

export interface DataItem {
  id: number;
  stdate: string;
  endate: string;
  logo?: string;
  name?: string;
  address: string;
  company: string;
  status: string;
}

export interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder | null;
  sortFn?: NzTableSortFn | null;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn | null;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
  width?: string;
  align?: string
}

export const tableCol: ColumnItem[] = [
  {
    name: 'Date',
    width: '20%',
    align: 'left',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.stdate.localeCompare(b.stdate),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Company',
    width: '20%',
    align: 'left',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.company.localeCompare(b.company),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Address',
    align: 'left',
  },
  {
    name: 'Status',
    width: '15%',
    align: 'left',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.status.localeCompare(b.status),
    sortDirections: ['ascend', 'descend', null],
    filterMultiple: true,
    listOfFilter: [
      { text: 'Active', value: 'active' },
      { text: 'Trial', value: 'trial' },
      { text: 'InActive', value: 'inactive' },
    ],
    filterFn: (list: string[], item: DataItem) => list.some(status => item.status.indexOf(status) !== -1)
  },
  {
    name: 'Action',
    width: '15%',
    align: 'left',
  }
];

export const tableData:DataItem[] = [
  {
    id: 1,
    stdate: '15/10/2015',
    endate: '31/01/2020',
    company: 'PT. Penjamin Infrastruktur Indonesia',
    address: 'Sampoerna Strategic Square, North Tower, 14th Floor. Jl. Jenderal Sudirman Kav. 45-46 Jakarta-Indonesia',
    status: 'active'
  },
  {
    id: 2,
    stdate: '18/06/2015',
    endate: '31/01/2020',
    logo: '../../../../assets/img/client/indosat.png',
    company: 'PT. INDOSAT TBK',
    address: 'Jl. Medan Merdeka Barat No. 21 Gambir, Jakarta Pusat, Kode Pos 10110 Indonesia',
    status: 'active'
  },
  {
    id: 3,
    stdate: '06/09/2015',
    endate: '31/01/2020',
    logo: '../../../../assets/img/client/bri.png',
    company: 'PT. Bank Rakyat Indonesia (Persero) Tbk.',
    address: 'Gedung BRI 1 Jl. Jenderal Sudirman Kav.44-46 Jakarta 10210 Indonesia',
    status: 'trial'
  }
];

