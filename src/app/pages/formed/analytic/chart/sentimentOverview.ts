import { lineOptions } from '@core/charts/line/linechart';
import { mockData1, mockData2, mockData3, mockData4 } from '@core/data/widgetData';

export const sentimentOverview = {
  chart: lineOptions.chart,
  tooltip: lineOptions.tooltip,
  grid: lineOptions.grid,
  markers: lineOptions.markers,
  stroke: lineOptions.stroke,
  dataLabels: lineOptions.dataLabels,
  colors:[ '#06d6a0', '#1990ff', '#ff6b6b' ],
  series: [
    {
      name: "Positive",
      data: mockData1,
    },
    {
      name: 'Neutral',
      data: mockData3,
    },
    {
      name: "Negative",
      data: mockData4,
    },
  ],
  title: {
    text: "Sentiment Overview",
    align: "left"
  },
  legend: {
    itemMargin: lineOptions.legend.itemMargin,
    fontWeight: lineOptions.legend.fontWeight,
    position: 'top',
    horizontalAlign: 'right',
    tooltipHoverFormatter: function(val, opts) {
      return (
        val +
        " - <strong>" +
        opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] +
        "</strong>"
      );
    }
  },
  xaxis: {
    labels: {
      trim: true,
      rotate: 0
    },
    categories: [
      "01 Jan",
      "02 Jan",
      "03 Jan",
      "04 Jan",
      "05 Jan",
      "06 Jan",
      "07 Jan",
      "08 Jan",
      "09 Jan",
      "10 Jan",
      "11 Jan",
      "12 Jan"
    ]
  },
  responsive: [
    {
      breakpoint: 1025,
      options: {
        chart: {
          height: 264
        },
      }
    },
  ]
};
