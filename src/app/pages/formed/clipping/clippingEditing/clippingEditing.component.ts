import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

import { DataItem, tableCol, tableData } from './tableOptions';

import { lorem500 } from '@constant/loremIpsum';

@Component({
  selector: 'app-clippingEditing',
  templateUrl: './clippingEditing.component.html',
  styleUrls: ['./clippingEditing.component.scss']
})
export class ClippingEditingComponent implements OnInit {
  lorem = lorem500;

  checked = false;
  indeterminate = false;
  isDetail = false;
  isSummary = false;

  listOfCurrentPageData: DataItem[] = [];
  setOfCheckedId = new Set<number>();
  expandSet = new Set<number>();

  listOfColumn = tableCol;
  listOfData: DataItem[] = tableData;

  buttonItem = [
    {
      text: 'send article',
      icon: 'mail'
    },
    {
      text: 'delete',
      icon: 'delete'
    },
  ];

  badge = [
    { label: 'category name 1'},
    { label: 'category name 2'},
    { label: 'category name 3' },
    { label: 'category name 4'},
    { label: 'category name 5'},
    { label: 'category name 6'}
  ]

  deleteRow(id: number): void {
    this.listOfData = this.listOfData.filter((d) => d.id !== id);
    this.messageService.info('data has been deleted');
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach((item) =>
      this.updateCheckedSet(item.id, value)
    );
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: DataItem[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every((item) =>
      this.setOfCheckedId.has(item.id)
    );
    this.indeterminate =
      this.listOfCurrentPageData.some((item) =>
        this.setOfCheckedId.has(item.id)
      ) && !this.checked;
  }


  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }
  constructor(private messageService: NzMessageService) { }

  ngOnInit() {

  }

  openDetail(): void {
    this.isDetail = true
  }

  closeDetail(): void {
    this.isDetail = false
  }

  openSummary(): void{
    this.isSummary = true
  }

  closeSummary(): void{
    this.isSummary = false
  }

}
