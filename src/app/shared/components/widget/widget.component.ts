import { Component, Input, OnInit } from '@angular/core';

enum InfoType {
  Positive = 'positive',
  Neutral = 'neutral',
  Negative = 'negative',
}

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {
  @Input() title: string;
  @Input() total: string;
  @Input() percent: string;

  @Input() icon: string;
  @Input() periode: string;
  @Input() class: string;

  @Input() infoIcon: string;

  get InfoType() {
    switch (this.class) {
      case InfoType.Positive:
        return 'positive';
      case InfoType.Neutral:
        return 'neutral';
      case InfoType.Negative:
        return 'negative';
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
