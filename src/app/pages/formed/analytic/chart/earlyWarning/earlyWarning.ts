import { lineOptions } from '@core/charts/line/linechart';
import { setData1, setData2, setData3, setData4, setData5 } from '@core/data/widgetData';


export const ewsOptions = {
  chart: {
    type: "line",
    height: 350,
    width: "100%",
    zoom: lineOptions.chart.zoom,
    dropShadow: lineOptions.chart.dropShadow,
    toolbar:lineOptions.chart.toolbar
  },
  tooltip: lineOptions.tooltip,
  grid: lineOptions.grid,
  markers: lineOptions.markers,
  stroke: {
    curve: 'straight',
    lineCap: 'round',
    width: 3
  },
  colors: lineOptions.colors,
  dataLabels: lineOptions.dataLabels,
  series: [
    {
      name: "Aksi Korporasi",
      data: setData1
    },
    {
      name: "Tol. Cinere-Serpong",
       data: setData2
    },
    {
      name: "JSDA",
       data: setData3
    },
  ],
  legend: {
    itemMargin: lineOptions.legend.itemMargin,
    fontWeight: 600,
    position: 'bottom',
    horizontalAlign: 'center',
    tooltipHoverFormatter: function(val, opts) {
      return (
        val +
        " - <strong>" +
        opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] +
        "</strong>"
      );
    }
  },
  yaxis: {
    show: true,
    tickAmount: 3,
    min: 0,
    max: 20,
    labels: {
      formatter: function(value, index) {
        if (value == 0  && value < 5) {
            return "Potential";
        }else if (value > 5 && value < 9) {
          return "Emerging"
        } else if (value > 10 && value < 20) {
          return "Current"
        }else {
            return "Crisis";
        }
      }
    },
  },
  xaxis: {
    labels: {
      trim: false
    },
    categories: [
      "01 Jan",
      "02 Jan",
      "03 Jan",
      "04 Jan",
      "05 Jan",
      "06 Jan",
      "07 Jan",
      "08 Jan",
      "09 Jan",
      "10 Jan",
      "11 Jan",
      "12 Jan"
    ]
  },
  responsive: [
    {
      breakpoint: 1441,
      options: {
        chart: {
          height: 400
        },
      }
    },
  ]
}
