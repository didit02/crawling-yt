import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AntdModule } from '@shared/antd.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { HeaderComponent } from '@components/layout/header/header.component';
import { SidebarComponent } from '@components/layout/sidebar/sidebar.component';

import { ButtonComponent } from '@components/button/button.component';
import { IconComponent } from '@components/icon/icon.component';
import { ListComponent } from '@components/list/list.component';

import { CardComponent } from '@components/card/card.component';
import { ModalComponent } from '@components/modal/modal.component';
import { WidgetComponent } from '@components/widget/widget.component';

import { LabelComponent } from '@components/form/label/label.component';
import { TagComponent } from '@components/form/tag/tag.component';

import { InputComponent } from '@components/form/input/input.component';
import { SelectComponent } from '@components/form/select/select.component';
import { AutocompleteComponent } from '@components/form/autocomplete/autocomplete.component';


@NgModule({
  imports: [ CommonModule, AntdModule, NgApexchartsModule, ],
  exports: [
    AntdModule,
    NgApexchartsModule,
    HeaderComponent,
    SidebarComponent,
    ButtonComponent,
    IconComponent,
    CardComponent,
    ModalComponent,
    WidgetComponent,
    AutocompleteComponent,
    InputComponent,
    SelectComponent,
    LabelComponent,
    TagComponent,
    ListComponent,
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    ButtonComponent,
    IconComponent,
    CardComponent,
    ModalComponent,
    WidgetComponent,
    AutocompleteComponent,
    InputComponent,
    SelectComponent,
    LabelComponent,
    TagComponent,
    ListComponent,
  ],
})
export class SharedModule { }
