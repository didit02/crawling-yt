import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

import { tableCol, tableData } from '../tableOptions';

@Component({
  selector: 'app-userList',
  templateUrl: './userList.component.html',
  styleUrls: ['./userList.component.scss']
})
export class UserListComponent implements OnInit {
  checked = false;
  indeterminate = false;
  isDetail = false;

  isCreate = false;
  isCreateLoading = false;

  passwordVisible = false;
  CreatePassword?: string;
  ConfirmPassword?: string;

  listOfCurrentPageData = tableData;
  setOfCheckedId = new Set<number>();
  expandSet = new Set<number>();

  listOfColumn = tableCol;
  listOfData = tableData;

  buttonItem = [
    {
      text: 'download',
      icon: 'download',
      class: 'line-info'
    },
    {
      text: 'delete',
      icon: 'delete',
      class: 'line-danger'
    }
  ];

  companyOptions = [
    {
      label: 'company name 1',
      value: "company1"
    },
    {
      label: 'company name 2',
      value: "company2"
    },    {
      label: 'company name 3',
      value: "company3"
    },    {
      label: 'company name 4',
      value: "company4"
    },    {
      label: 'company name 5',
      value: "company5"
    },
  ]

  deleteRow(id: number): void {
    this.listOfData = this.listOfData.filter((d) => d.id !== id);
    this.messageService.info('data has been deleted');
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach((item) =>
      this.updateCheckedSet(item.id, value)
    );
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange(tableData): void {
    this.listOfCurrentPageData = tableData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every((item) =>
      this.setOfCheckedId.has(item.id)
    );
    this.indeterminate =
      this.listOfCurrentPageData.some((item) =>
        this.setOfCheckedId.has(item.id)
      ) && !this.checked;
  }


  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  constructor(private messageService: NzMessageService) { }

  ngOnInit() {
  }

  openCreate(){
    this.isCreate = true;
  }

  closeCreate() {
    this.isCreate = false;
  }

  createSubmit(type: string) {
    this.isCreateLoading = true;

    setTimeout(() => {
      this.isCreate = false;
      this.isCreateLoading = false;
      this.messageService.create(type, `Create New User ${type}`);
    }, 3000);
  }

}
