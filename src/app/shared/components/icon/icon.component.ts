import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {
  @Input() class: string;
  @Input() spin: boolean;
  @Input() rotate: number;
  @Input() type: string | any;
  @Input() size: number;
  @Input() color: number | any;
  @Input() theme: string; // 'fill'|'outline'|'twotone'

  constructor() { }

  ngOnInit() {
  }

}
