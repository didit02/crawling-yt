import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';

export interface DataItem {
  id: number;
  date: string;
  name: string;
  userId: number;
  time: string;
  company: string;
  status: string;
}

export interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder | null;
  sortFn?: NzTableSortFn | null;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn | null;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
  width?: string;
  align?: string
}


export const tableCol: ColumnItem[] = [
  {
    name: 'Name',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.name.localeCompare(b.name),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Company',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.company.localeCompare(b.company),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Status',
    width: '20%',
    align: 'left',
    sortOrder: 'ascend',
    sortFn: (a: DataItem, b: DataItem) => a.status.localeCompare(b.status),
    sortDirections: ['ascend', 'descend', null],
    filterMultiple: true,
    listOfFilter: [
      { text: 'Admin', value: 'admin' },
      { text: 'Editor', value: 'editor' },
      { text: 'Reader', value: 'reader' },
      { text: 'Viewer', value: 'viewer' },
      { text: 'Custom', value: 'custom' },
    ],
    filterFn: (list: string[], item: DataItem) => list.some(status => item.status.indexOf(status) !== -1)
  }
];

export const tableData:DataItem[] = [
  {
    id: 1,
    date: '01/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 1,
    company: 'company name',
    status: 'admin',
  },
  {
    id: 2,
    date: '02/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 2,
    company: 'company name',
    status: 'reader',
  },
  {
    id: 3,
    date: '03/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 3,
    company: 'company name',
    status: 'reader',
  },
  {
    id: 4,
    date: '04/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 4,
    company: 'company name',
    status: 'admin',
  },

  {
    id: 5,
    date: '05/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 5,
    company: 'company name',
    status: 'admin',
  },
  {
    id: 6,
    date: '06/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 6,
    company: 'company name',
    status: 'viewer',
  },
  {
    id: 7,
    date: '07/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 7,
    company: 'company name',
    status: 'viewer',
  },
  {
    id: 8,
    date: '08/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 8,
    company: 'company name',
    status: 'editor',
  },

  {
    id: 9,
    date: '09/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 9,
    company: 'company name',
    status: 'editor',
  },
  {
    id: 10,
    date: '10/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 10,
    company: 'company name',
    status: 'editor',
  },
  {
    id: 11,
    date: '11/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 11,
    company: 'company name',
    status: 'admin',
  },
  {
    id: 12,
    date: '12/01/2020',
    time: '00:00',
    name: 'user name',
    userId: 12,
    company: 'company name',
    status: 'admin',
  },
];

