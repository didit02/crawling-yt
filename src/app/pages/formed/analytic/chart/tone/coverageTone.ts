import { barOptions } from '@core/charts/bar/barchart';
import { mockData1, mockData2, mockData3, mockData4 } from '@core/data/widgetData';

export const coverageTone = {
  colors:[ '#ff6b6b', '#1990ff', '#06d6a0'],
  series: [
    {
      name: "Negative",
      data: mockData1,
    },
    {
      name: 'Neutral',
      data: mockData3,
    },
    {
      name: "Positive",
      data: mockData4,
    },
  ],
  plotOptions: {
    bar: {
      horizontal: false
    }
  },
  chart: barOptions.chart,
  stroke: barOptions.stroke,
  xaxis: {
    categories: [
      "01 Jan",
      "02 Jan",
      "03 Jan",
      "04 Jan",
      "05 Jan",
      "06 Jan",
      "07 Jan",
      "08 Jan",
      "09 Jan",
      "10 Jan",
      "11 Jan",
      "12 Jan"
    ],
    labels: barOptions.xaxis.labels
  },
  yaxis: barOptions.yaxis,
  tooltip: barOptions.tooltip,
  fill: barOptions.fill,
  legend:barOptions.legend
}
