import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';

export interface DataItem {
  id: number;
  date: string;
  mediaId: number;
  time: string;
  mediaName: string;
  headline: string;
}

export interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder | null;
  sortFn?: NzTableSortFn | null;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn | null;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
  width?: string;
  align?: string
}


export const tableCol: ColumnItem[] = [
  {
    name: 'Date',
    align: 'left',
    width:'20%',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.date.localeCompare(b.date),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Media',
    align: 'left',
    width:'20%',
    sortOrder: null,
    sortFn: (a: DataItem, b: DataItem) => a.mediaName.localeCompare(b.mediaName),
    sortDirections: ['ascend', 'descend', null],
  },
  {
    name: 'Headline',
    width:'30%',
    align: 'left',
  }
];

export const tableData:DataItem[] = [
  {
    id: 1,
    date: '01/01/2020',
    time: '00:00',
    mediaId: 1,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 2,
    date: '02/01/2020',
    time: '00:00',
    mediaId: 2,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 3,
    date: '03/01/2020',
    time: '00:00',
    mediaId: 3,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 4,
    date: '04/01/2020',
    time: '00:00',
    mediaId: 4,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 5,
    date: '05/01/2020',
    time: '00:00',
    mediaId: 5,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 6,
    date: '06/01/2020',
    time: '00:00',
    mediaId: 6,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 7,
    date: '07/01/2020',
    time: '00:00',
    mediaId: 7,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 8,
    date: '08/01/2020',
    time: '00:00',
    mediaId: 8,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },

  {
    id: 9,
    date: '09/01/2020',
    time: '00:00',
    mediaId: 9,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 10,
    date: '10/01/2020',
    time: '00:00',
    mediaId: 10,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 11,
    date: '11/01/2020',
    time: '00:00',
    mediaId: 11,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
  {
    id: 12,
    date: '12/01/2020',
    time: '00:00',
    mediaId: 12,
    mediaName: 'medianews.com',
    headline: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim laudantium sequi a consequuntur sapiente veniam eum quam voluptate atque illum?',
  },
];

