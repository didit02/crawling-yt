import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from 'ng-apexcharts';

import { NzMessageService } from 'ng-zorro-antd/message';
import { tableData } from './tableOptions';
import { contentOpt, mediaOpt, subMediaOpt, categoryOpt, subCategoryOpt } from './filterOptions';

import { sentimentOverview } from '@pages/formed/analytic/chart/sentimentOverview';

// Early Warning System
import { ewsOptions } from '@pages/formed/analytic/chart/earlyWarning/earlyWarning';

// Visibility
import { mediaVisibility } from '@pages/formed/analytic/chart/visibility/mediaVisibility';
import { mediaPie } from '@pages/formed/analytic/chart/visibility/mediaPie';

// Tone
import { coverageTone } from '@pages/formed/analytic/chart/tone/coverageTone';
import { mediaSelection } from '@pages/formed/analytic/chart/tone/mediaSelection';
import { toneCategory } from '@pages/formed/analytic/chart/tone/toneCategory';

import { lorem500 } from '@constant/loremIpsum';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;

  lorem = lorem500;

  checked = false;
  indeterminate = false;
  isDetail = false;

  // Filter
  isFilter = false;
  isConfirmLoading = false;
  ranges = {
    Today: [new Date(), new Date()],
    Month: [new Date(), endOfMonth(new Date())]
  };

  // Filter - select
  contentOptions = contentOpt;
  mediaOptions = mediaOpt;
  subMediaOptions = subMediaOpt;
  categoryOptions = categoryOpt;
  subCategoryOptions = subCategoryOpt;

  listOfData = tableData;

  public widgetOptions = sentimentOverview;
  // Early Warning System
  public earlyOptions = ewsOptions;

  // Visibility
  public visibilityOptions = mediaVisibility;
  public pieOptions = mediaPie;

  // Tone
  public coverageOptions = coverageTone;
  public selectionOptions = mediaSelection;
  public toneCategoryOptions = toneCategory;

  widgetData = [
    {
      title: "positive mention",
      icon: "plus-circle",
      total: "36,254",
      percent: "5.20",
      periode: "week",
      infoType: 'positive',
      infoIcon: 'arrow-up'
    },
    {
      title: "Neutral mention",
      icon: "minus-circle",
      total: "36,254",
      percent: "5.20",
      periode: "week",
      infoType: 'neutral',
      infoIcon: 'minus'
    },
    {
      title: "Negative mention",
      icon: "close-circle",
      total: "36,254",
      percent: "5.20",
      periode: "week",
      infoType: 'negative',
      infoIcon: 'arrow-down'
    },
  ];

  constructor(private messageService: NzMessageService) {
    this.widgetOptions = sentimentOverview;
    // Early Warning System
    this.earlyOptions = ewsOptions;

    // Visibility
    this.visibilityOptions = mediaVisibility;
    this.pieOptions = mediaPie;

    // Tone
    this.coverageOptions = coverageTone;
    this.selectionOptions = mediaSelection;
    this.toneCategoryOptions = toneCategory;
  }

  ngOnInit() {
  }

  onRange(result: Date[]): void {
    console.log('From: ', result[0], ', to: ', result[1]);
  }

  openFilter() {
    this.isFilter = true;
  }

  submitFilter(type: string) {
    this.isConfirmLoading = true;

    setTimeout(() => {
      this.isFilter = false;
      this.isConfirmLoading = false;
      this.messageService.create(type, `filter setting ${type}`);
    }, 3000);
  }

  closeFilter() {
    this.isFilter = false;
  }
}
