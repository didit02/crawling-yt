import {
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexLegend,
  ApexResponsive,
  ApexYAxis,
  ApexGrid,
  ApexAnnotations,
  ApexDataLabels,
  ApexNonAxisChartSeries,
  ApexStroke,
  ApexFill,
  ApexTooltip,
  ApexPlotOptions,
  ApexStates,
  ApexTheme,
  ApexMarkers,
  ApexOptions,
} from 'ng-apexcharts';

export interface ApexChartOptions {
  chart: ApexChart;
  annotations: ApexAnnotations;
  colors: string[];
  dataLabels: ApexDataLabels;
  series: ApexAxisChartSeries | ApexNonAxisChartSeries;
  stroke: ApexStroke;
  labels: string[] | any;
  legend: ApexLegend;
  fill: ApexFill | any;
  tooltip: ApexTooltip | any;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  grid: ApexGrid;
  states: ApexStates;
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  theme: ApexTheme;
  markers: ApexMarkers;
  options: ApexOptions[] | any;
}
