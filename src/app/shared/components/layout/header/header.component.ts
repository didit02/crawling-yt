import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() collapsed: boolean;
  @Input() logo: string;

  @Input() notifMenu: string;
  @Output() click = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
