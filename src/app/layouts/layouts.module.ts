import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { QuillModule } from 'ngx-quill';

import { LayoutsRoutingModule } from '@layouts/layouts-routing.module';

import { ExampleComponent } from '@pages/example/example.component';
import { DefaultComponent } from '@layouts/default/default.component';

import { AuthorComponent } from '@layouts/author/author.component';
import { DashboardComponent } from '@pages/formed/analytic/dashboard/dashboard.component';

import { ClippingListComponent } from '@pages/formed/clipping/clippingList/clippingList.component';
import { ClippingEditingComponent } from '@pages/formed/clipping/clippingEditing/clippingEditing.component';
import { ClippingSearchComponent } from '@pages/formed/clipping/clippingSearch/clippingSearch.component';

import { ConnectComponent } from '@pages/formed/connect/connect.component';

import { CategoryComponent } from '@pages/formed/category/category.component';
import { GroupMediaComponent } from '@pages/formed/category/table/groupMedia/groupMedia.component';
import { GroupCategoryComponent } from '@pages/formed/category/table/groupCategory/groupCategory.component';
import { GroupSubCategoryComponent } from '@pages/formed/category/table/groupSubCategory/groupSubCategory.component';

import { UserListComponent } from '@pages/admin/user/userList/userList.component';
import { CompanyListComponent } from '@pages/admin/company/companyList/companyList.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    LayoutsRoutingModule,
    QuillModule.forRoot(),
  ],
  exports: [FormsModule],
  declarations: [
    DefaultComponent,
    AuthorComponent,
    DashboardComponent,
    ExampleComponent,
    ClippingListComponent,
    ClippingEditingComponent,
    ClippingSearchComponent,
    ConnectComponent,
    CategoryComponent,
    GroupMediaComponent,
    GroupCategoryComponent,
    GroupSubCategoryComponent,
    UserListComponent,
    CompanyListComponent,
  ],
})
export class LayoutsModule { }
