import { lineOptions } from '@core/charts/line/linechart';
import { mockData1, mockData2, mockData3, mockData4 } from '@core/data/widgetData';

export const mediaVisibility = {
  chart: {
    type: "line",
    height: 350,
    width: "100%",
    zoom: lineOptions.chart.zoom,
    dropShadow: lineOptions.chart.dropShadow,
    toolbar:lineOptions.chart.toolbar
  },
  tooltip: lineOptions.tooltip,
  grid: lineOptions.grid,
  markers: lineOptions.markers,
  stroke: {
    curve: 'straight',
    lineCap: 'round',
    width: 3
  },
  colors: lineOptions.colors,
  dataLabels: lineOptions.dataLabels,
  series: [
    {
      name: "Aksi Korporasi",
      data: mockData1
    },
    {
      name: "Tol. Cinere-Serpong",
      data: mockData2
    },
    {
      name: "JSDA",
      data: mockData3
    },
    {
      name: "Tol. Jakarta-Cikampek",
      data: mockData4
    },
  ],
  legend: {
    itemMargin: lineOptions.legend.itemMargin,
    fontWeight: 600,
    position: 'bottom',
    horizontalAlign: 'center',
    tooltipHoverFormatter: function(val, opts) {
      return (
        val +
        " - <strong>" +
        opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] +
        "</strong>"
      );
    }
  },
  xaxis: {
    labels: {
      trim: false
    },
    categories: [
      "01 Jan",
      "02 Jan",
      "03 Jan",
      "04 Jan",
      "05 Jan",
      "06 Jan",
      "07 Jan",
      "08 Jan",
      "09 Jan",
      "10 Jan",
      "11 Jan",
      "12 Jan"
    ]
  }
};
