export const MediaItem = [
  {
    title: 'Berita Online',
    key: 'online',
    children: [
      { title: 'Berita Online 1', key: 'online-1', isLeaf: true },
      { title: 'Berita Online 2', key: 'online-2', isLeaf: true },
      { title: 'Berita Online 3', key: 'online-3', isLeaf: true },
    ]
  },
  {
    title: 'Koran',
    key: 'koran',
    children: [
      {
        title: 'Koran Daerah',
        key: 'koran-1',
        children: [
          { title: 'Koran Daerah 1', key: 'koran-1-1', isLeaf: true },
          { title: 'Koran Daerah 2', key: 'koran-1-2', isLeaf: true },
        ]
      },
      {
        title: 'Koran Nasional',
        key: 'koran-2',
        children: [
          { title: 'Koran Nasional 1', key: 'koran-2-1', isLeaf: true },
          { title: 'Koran Nasional 2', key: 'koran-2-2', isLeaf: true },
        ]
      },
    ]
  },
  {
    title: 'Majalah',
    key: 'majalah',
    children: [
      {
        title: 'Majalah Mingguan',
        key: 'majalah-1',
        children: [
          { title: 'Majalah Mingguan 1', key: 'majalah-1-1', isLeaf: true },
          { title: 'Majalah Mingguan 2', key: 'majalah-1-2', isLeaf: true },
        ]
      },
      {
        title: 'Majalah Dwi Mingguan',
        key: 'majalah-2',
        children: [
          { title: 'Majalah Dwi Mingguan 1', key: 'majalah-2-1', isLeaf: true },
          { title: 'Majalah Dwi Mingguan 2', key: 'majalah-2-2', isLeaf: true },
        ]
      },
      {
        title: 'Majalah Bulanan',
        key: 'majalah-3',
        children: [
          { title: 'Majalah Bulanan 1', key: 'majalah-3-1', isLeaf: true },
          { title: 'Majalah Bulanan 2', key: 'majalah-3-2', isLeaf: true },
        ]
      }
    ]
  },
  {
    title: 'Tabloid',
    key: 'tabloid',
    children: [
      { title: 'Tabloid 1', key: 'tabloid-1', isLeaf: true },
      { title: 'Tabloid 2', key: 'tabloid-2', isLeaf: true },
      { title: 'Tabloid 3', key: 'tabloid-3', isLeaf: true },
    ]
  },
  {
    title: 'Radio',
    key: 'radio',
    children: [
      { title: 'Radio 1', key: 'radio-1', isLeaf: true },
      { title: 'Radio 2', key: 'radio-2', isLeaf: true },
      { title: 'Radio 3', key: 'radio-3', isLeaf: true },
    ]
  },
  {
    title: 'Televisi',
    key: 'tv',
    children: [
      { title: 'Televisi 1', key: 'tv-1', isLeaf: true },
      { title: 'Televisi 2', key: 'tv-2', isLeaf: true },
      { title: 'Televisi 3', key: 'tv-3', isLeaf: true },
    ]
  },
];
