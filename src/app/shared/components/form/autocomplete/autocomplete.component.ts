import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

export interface AutocompleteOptionGroups {
  title: string;
  count?: number;
  children?: AutocompleteOptionGroups[];
}

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {
  @Input() inputValue?: string;
  @Input() optionGroups: AutocompleteOptionGroups[] = [];

  constructor() { }

  onChange(value: string): void {
    console.log(value);
  }

  ngOnInit() {
    setTimeout(() => {
      this.optionGroups = [
        {
          title: 'title',
          children: [
            {
              title: 'AntDesign',
              count: 10000
            },
            {
              title: 'AntDesign UI',
              count: 10600
            }
          ]
        },
        {
          title: 'title',
          children: [
            {
              title: 'AntDesign UI',
              count: 60100
            },
            {
              title: 'AntDesign',
              count: 30010
            }
          ]
        },
        {
          title: 'title',
          children: [
            {
              title: 'AntDesign',
              count: 100000
            }
          ]
        }
      ];
    }, 1000);
  }

}
