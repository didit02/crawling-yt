export const MenuItem = [
  {
    level: 1,
    title: 'Formal Media',
    icon: 'desktop',
    collapse: true,
    children: [
      {
        level: 2,
        title: 'Analytic',
        icon: 'dashboard',
        path:'dashboard',

      },
      {
        level: 2,
        title: 'Group Category',
        icon: 'folder',
        path:'category',
      },
      {
        level: 2,
        title: 'Media Connect',
        icon: 'mail',
        path:'connect',
      },
      {
        level: 2,
        title: 'News Clipping',
        icon: 'container',
        collapse: true,
        children: [
          {
            level: 3,
            title: 'News List',
            path: 'news-clipping/list',
          },
          {
            level: 3,
            title: 'News Editing',
            path: 'news-clipping/editing',
          },
          {
            level: 3,
            title: 'News Search',
            path: 'news-clipping/search',
          }
        ]
      }
    ]
  },
  {
    level: 1,
    title: 'Administrator',
    icon: 'setting',
    collapse: true,
    children: [
      {
        level: 2,
        title: 'User List',
        icon: 'team',
        path:'administrator/user_list',
      },
      {
        level: 2,
        title: 'Company List',
        icon: 'shop',
        path:'administrator/company_list',
      },
      {
        level: 2,
        title: 'Example',
        icon: 'layout',
        path:'#',
      },
    ]
  }
];
