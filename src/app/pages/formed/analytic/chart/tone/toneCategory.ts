import { barOptions } from '@core/charts/bar/barchart';
import { mockData1, mockData2, mockData3, mockData4 } from '@core/data/widgetData';

export const toneCategory = {
  colors:[ '#ff6b6b', '#1990ff', '#06d6a0'],
  series: [
    {
      name: "Negative",
      data: mockData1,
    },
    {
      name: 'Neutral',
      data: mockData3,
    },
    {
      name: "Positive",
      data: mockData4,
    },
  ],
  plotOptions: {
    bar: {
      horizontal: false
    }
  },
  chart: barOptions.chart,
  stroke: barOptions.stroke,
  xaxis: {
    categories: ['category', 'category', 'category', 'category', 'category', 'category', 'category', 'category', 'category', 'category'],
    labels: barOptions.xaxis.labels
  },
  yaxis: barOptions.yaxis,
  tooltip: barOptions.tooltip,
  fill: barOptions.fill,
  legend:barOptions.legend
}
