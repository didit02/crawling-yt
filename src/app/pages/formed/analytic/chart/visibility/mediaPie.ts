import { pieOptions } from '@core/charts/pie/pieOptions';
import { mockData1 } from '@core/data/widgetData';

export const mediaPie = {
  series: [44, 55, 13, 43, 22],
  chart: {
    width: 480,
    type: "donut"
  },
  colors: pieOptions.colors,
  legend: {
    fontWeight: 600,
    position: "bottom",
    horizontalAlign: 'center',
  },
  dataLabels: pieOptions.dataLabels,
  responsive: [
    {
      breakpoint: 769,
      options: {
        chart: {
          width: 400
        },
        legend: {
          horizontalAlign: 'center',
        },
      }
    },
    {
      breakpoint: 1025,
      options: {
        chart: {
          width: 360
        },
        legend: {
          horizontalAlign: 'left',
        },
      }
    },
  ],
};
