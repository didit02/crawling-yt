import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorComponent } from '@layouts/author/author.component';
import { DefaultComponent } from './default/default.component';

import { DashboardComponent } from '@pages/formed/analytic/dashboard/dashboard.component';
import { ClippingListComponent } from '@pages/formed/clipping/clippingList/clippingList.component';
import { ClippingEditingComponent } from '@pages/formed/clipping/clippingEditing/clippingEditing.component';
import { ClippingSearchComponent } from '@pages/formed/clipping/clippingSearch/clippingSearch.component';

import { ConnectComponent } from '@pages/formed/connect/connect.component';
import { CategoryComponent } from '@pages/formed/category/category.component';
import { ExampleComponent } from '@pages/example/example.component';
import { UserListComponent } from '@pages/admin/user/userList/userList.component';
import { CompanyListComponent } from '@pages/admin/company/companyList/companyList.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/search-engine' },
  {
    path: 'search-engine',
    component: AuthorComponent
  },
  // {
  //   path: '',
  //   component: DefaultComponent,
  //   children: [
  //     {
  //       path: 'dashboard',
  //       component: DashboardComponent,
  //     },
  //     {
  //       path: 'category',
  //       component: CategoryComponent,
  //     },
  //     {
  //       path: 'connect',
  //       component: ConnectComponent,
  //     },
  //     {
  //       path: 'news-clipping',
  //       children: [
  //         {
  //           path: 'list',
  //           component: ClippingListComponent,
  //         },
  //         {
  //           path: 'editing',
  //           component: ClippingEditingComponent,
  //         },
  //         {
  //           path: 'search',
  //           component: ClippingSearchComponent,
  //         },
  //       ]
  //     },
  //     {
  //       path: 'administrator',
  //       children: [
  //         {
  //           path: "user_list",
  //           component: UserListComponent,
  //         },
  //         {
  //           path: 'company_list',
  //           component: CompanyListComponent,
  //         }
  //       ]
  //     },
  //     {
  //       path: 'example',
  //       component: ExampleComponent,
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
