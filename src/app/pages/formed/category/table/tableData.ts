export interface DataItem {
  id: number;
  mediaGroup?: string;
  category?: string;
  subCategory?: string;
}

export const groupMediaData: DataItem[] = [
  {
    id: 1,
    mediaGroup:'All Media'
  },
  {
    id: 2,
    mediaGroup:'Media Cetak'
  },
  {
    id: 3,
    mediaGroup:'Media Online'
  },
  {
    id: 4,
    mediaGroup:'Media TV'
  },
  {
    id: 5,
    mediaGroup:'Media Radio'
  }
];

export const GroupCategoryData: DataItem[] = [
  {
    id: 1,
    category:'Jasa Marga'
  },
  {
    id: 2,
    category:'JMT'
  },
  {
    id: 3,
    category:'JTT'
  },
  {
    id: 4,
    category:'JNT'
  },
  {
    id: 5,
    category:'Regulator & Stakeholder'
  },
  {
    id: 6,
    category:'Kompetitor BUMN Karya'
  },
  {
    id: 7,
    category:'Kompetitor Perhubungan'
  },
  {
    id: 8,
    category:'Isu Khusus'
  },
  {
    id: 9,
    category: 'Category New 1'
  },
  {
    id: 10,
    category: 'Category New 2'
  }
];

export const groupSubCategoryData: DataItem[] = [
  {
    id: 1,
    subCategory:'Aksi Korporasi	'
  },
  {
    id: 2,
    subCategory:'JCI'
  },
  {
    id: 3,
    subCategory:'JMRB'
  },
  {
    id: 4,
    subCategory:'JMTM'
  },
  {
    id: 5,
    subCategory:'JMTO'
  },
  {
    id: 6,
    subCategory:'JSDA'
  },
  {
    id: 7,
    subCategory:'Pengembangan Usaha'
  },
  {
    id: 8,
    subCategory:'Tol Cinere-Serpong'
  },
  {
    id: 9,
    subCategory: 'Tol Jakarta-Cikampek Elevated	'
  },
  {
    id: 10,
    subCategory:'Tol Kunciran-Serpong'
  }
];
